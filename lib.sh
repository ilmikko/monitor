#!/usr/bin/env bash

OK() {
	echo "OK $@" 1>&2;
}

FAIL() {
	echo "FAIL $@" 1>&2;
	exit 1;
}

allow() {
	cat | grep -v "^#" | awk '{ if ($0) print "ALLOW "$0 }';
}

deny() {
	cat | grep -v "^#" | awk '{ if ($0) print "DENY "$0 }';
}

gather_dirs() {
	for dir in "$1/"*; do
		[ -d "$dir" ] || continue;
		gather "$dir" || FAIL "$(pwd)/$dir";
	done
}

# gather gathers deny/allow rules from a directory.
gather() {
	while [ -n "$1" ]; do
		(
		cd "$1";
		source "./GATHER" || FAIL "$(pwd)/GATHER";
		) || return 1;
		shift
	done
}

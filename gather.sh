#!/usr/bin/env bash
# Gather files in a trimmed root tree.

DIR="$(dirname $0)";
. "$DIR/lib.sh" || exit 222;

findify() {
	printf "find / ";
	grep -v "^#" | awk '{ if ($1 == "DENY") {not="-not"} else {not=""}; $1=""; gsub(/^[ \t]+|[ \t]+$)/, "", $0); print not" \\( -path \""$0"\" -prune \\) \\" }';
}

remove_dirs() {
	while read line; do
		[ -d "$line" ] && continue;
		echo "$line";
	done
}

trim() {
	RULES=$(gather root) || FAIL "Gather failed.";
	EXTRA=$(echo "$RULES" | findify | bash);
	echo "$EXTRA" | remove_dirs;
}

trim;
# trim 2>/dev/null | awk -F/ '{ print $2"/"$3"/"$4"/"$5 }' | sort | uniq -c | sort -h | grep -v "^\s*1";

#!/usr/bin/env bash
# Gitter is a small tool to check the status of a directory of git directories.

cd "$1";

all_up_to_date=1;

for folder in ./*; do
	[ -d "$folder" ] || continue;

	(
	cd "$folder";

	if git status >/dev/null 2>&1; then
		commits=" ";
		if ! git status | grep --silent 'up to date'; then
			commits=$(git status | grep --perl-regexp --only 'by \d+ commits?' | awk '{ print $2 }');
		fi
		if [ -z "$(git status --porcelain)" ]; then
			echo "[32;1m$commits $folder[m" 1>&2
			exit 0;
		elif [ -z "$(git status --porcelain | grep -v "^??")" ]; then
			echo "[33;1m$commits $folder[m";
			exit 1;
		else
			echo "[31;1m$commits $folder[m";
			exit 1;
		fi
	fi
	) || all_up_to_date=0;
done

[ "$all_up_to_date" = "1" ] || exit 1;
